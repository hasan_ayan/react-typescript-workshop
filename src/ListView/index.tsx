import React from "react";
import { Accordion } from "../Accordion";

export type ListViewItem = {
  header: string;
  items: string[];
};

type ListViewProps = {
  items: ListViewItem[];
};

type ListViewChildrenProps = {
  list: string[];
};

const ListViewChildren = ({ list }: ListViewChildrenProps) => {
  return (
    <ul>
      {list.map((child) => (
        <li>{child}</li>
      ))}
    </ul>
  );
};

export const ListView = ({ items }: ListViewProps) => {
  return (
    <>
      {items.map((item) => (
        <Accordion header={item.header}>
          <ListViewChildren list={item.items} />
        </Accordion>
      ))}
    </>
  );
};
