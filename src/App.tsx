import React from "react";
import logo from "./logo.svg";
import "./App.css";
import { Accordion } from "./Accordion";
import { ListView, ListViewItem } from "./ListView2";

const shoppingList: ListViewItem[] = [
  {
    header: "grocery",
    items: ["apples", "banana", "oranges"],
  },
  {
    header: "clothes",
    items: ["t-shirt", "skirt", "socks"],
  },
  {
    header: "cars",
    items: ["bmw", "honda", "toyota"],
  },
];

const HeaderContent = () => (
  <div style={{ display: "inline-block" }}>
    <img src={logo} height="20" />
    <span>header</span>
  </div>
);

function App() {
  const inputRef = React.useRef<HTMLInputElement>(null);
  return (
    <div className="App">
      <ListView items={shoppingList} />
    </div>
  );
}

export default App;
