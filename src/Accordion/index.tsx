import React, {
  useState,
  useCallback,
  PropsWithChildren,
  ReactNode,
} from "react";

type AccordionProps = PropsWithChildren<{
  header: ReactNode;
}>;

export const Accordion = ({ header, children }: AccordionProps) => {
  const [isOpen, setIsOpen] = useState(false);

  const onClick = useCallback(() => {
    setIsOpen((state) => !state);
  }, []);

  return (
    <div>
      <div onClick={onClick}>
        <span>[{isOpen ? "-" : "+"}]</span>
        <span>{header}</span>
      </div>
      {isOpen && <div>{children}</div>}
    </div>
  );
};
