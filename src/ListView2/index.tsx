import React from "react";
import { Accordion } from "../Accordion";
import { ControlledAccordion } from "../ControlledAccordion";

export type ListViewItem = {
  header: string;
  items: string[];
};

type ListViewProps = {
  items: ListViewItem[];
};

type ListViewChildrenProps = {
  list: string[];
};

const ListViewChildren = ({ list }: ListViewChildrenProps) => {
  return (
    <ul>
      {list.map((child) => (
        <li>{child}</li>
      ))}
    </ul>
  );
};

export const ListView = ({ items }: ListViewProps) => {
  const [openItem, setOpenItem] = React.useState("");

  return (
    <>
      {items.map((item) => (
        <ControlledAccordion
          header={item.header}
          isOpen={openItem === item.header}
          onClick={() => {
            setOpenItem(item.header);
          }}
        >
          <ListViewChildren list={item.items} />
        </ControlledAccordion>
      ))}
    </>
  );
};
