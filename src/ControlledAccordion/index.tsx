import React, {
  useState,
  useCallback,
  PropsWithChildren,
  ReactNode,
} from "react";

type AccordionProps = PropsWithChildren<{
  header: ReactNode;
  onClick: () => any;
  isOpen?: boolean;
}>;

export const ControlledAccordion = ({
  header,
  children,
  onClick,
  isOpen,
}: AccordionProps) => {
  return (
    <div>
      <div onClick={onClick}>
        <span>[{isOpen ? "-" : "+"}]</span>
        <span>{header}</span>
      </div>
      {isOpen && <div>{children}</div>}
    </div>
  );
};
